
const mongoose = require('mongoose');
const moment = require('moment');
const axios = require("axios");
const schedule = require('node-schedule');
var url = 'http://192.168.56.30:2375';


//Mongo db client library
//const MongoClient  = require('mongodb');

//Express web service library
const express = require('express')

//used to parse the server response from json to object.
const bodyParser = require('body-parser');

//instance of express and port to use for inbound connections.
const app = express()
const port = 3000

var os = require("os");
const { allowedNodeEnvironmentFlags } = require('process');
const { time } = require('console');
var myhostname = os.hostname();
let nodes = [];
//connection string listing the mongo servers. This is an alternative to using a load balancer. THIS SHOULD BE DISCUSSED IN YOUR ASSIGNMENT.
const connectionString = 'mongodb://localmongo1:27017,localmongo2:27017,localmongo3:27017/NotFLIX?replicaSet=rs0';

setInterval(function() {

  console.log(`Intervals are used to fire a function for the lifetime of an application.`);

}, 3000);




//tell express to use the body parser. Note - This function was built into express but then moved to a seperate package.
app.use(bodyParser.json());

//connect to the cluster
mongoose.connect(connectionString, {useNewUrlParser: true, useUnifiedTopology: true});


var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

var Schema = mongoose.Schema;

var clientsSchema = new Schema({
  AccountId: Number,
  userName: String,
  title_id: Number,
  userAction: String,
  point_interaction: String,
  type_interaction: Number,
  date_time: Date
});

var clientsSchemal = mongoose.model('Client', clientsSchema, 'Client');



app.get('/', (req, res) => {
  clientsModel.find({},'item price quantity lastName', (err, client) => {
    if(err) return handleError(err);
    res.send(JSON.stringify(client))
  }) 
})

app.post('/',  (req, res) => {
  var awesome_instance = new SomeModel(req.body);
  awesome_instance.save(function (err) {
  if (err) res.send('Error');
    res.send(JSON.stringify(req.body))
  });
})

app.put('/',  (req, res) => {
  res.send('Got a PUT request at /')
})

app.delete('/',  (req, res) => {
  res.send('Got a DELETE request at /')
})

//bind the express web service to the port specified
app.listen(port, () => {
 console.log(`Express Application listening at port ` + port)
})

var currentTime = moment()

var nodeID= Math.floor(Math.random() * (100 - 1 + 1) + 1);
toSend = {"hostname": myhostname, "status": "alive", "nodeID": nodeID, "time": currentTime};




setInterval(function() {

var amqp = require('amqplib/callback_api');


amqp.connect('amqp://test:test@cloud-course-work_haproxy_1', function(error0, connection) {
      if (error0) {
              throw error0;
            }
      connection.createChannel(function(error1, channel) {
              if (error1) {
                        throw error1;
                      }
              var queue = 'hello';
              

              channel.assertQueue(queue, {
                        durable: false
                      });

              //channel.sendToQueue(queue, Buffer.from(msg));
              channel.sendToQueue(queue, Buffer.from(JSON.stringify(toSend)));
              console.log(" [x] Sent %s", JSON.stringify(toSend));
            });
    setTimeout(function() {
              connection.close();
              //process.exit(0)
              }, 500);
});

}, 1000);
setInterval(function(){sub()},5000 );

function sub(){

var amqp = require('amqplib/callback_api');

amqp.connect('amqp://test:test@cloud-course-work_haproxy_1', function(error0, connection) {
        if (error0) {
                    throw error0;
                }
        connection.createChannel(function(error1, channel) {
                    if (error1) {
                                    throw error1;
                                }

                    var queue = 'hello';

                    channel.assertQueue(queue, {
                                    durable: false
                                });

                    console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", queue);

                    channel.consume(queue, function(msg) {
                                    console.log(" [x] Received %s", msg.content.toString());
                                    const received = msg.content.toString();
                                    const container = JSON.parse(received);
                                    const attribute = nodes.hasOwnProperty('dead'); 
                                    
                                    // check if a node has dead, it will receive a message with the id that should be deleted
                                    if(attribute == true){
                                      console.log("This will be deleted:", container)
                                      nodes.splice(nodes.findIndex(function(i){
                                        return i.nodeID === container.dead;
                                    }), 1);
                                    }
                                    
                                    // update the time of the node, we are using moment library, since it is quite useful for time
                                    else if(nodes.some( i => i.nodeID === container.nodeID) && nodes.some( j => j.hostname === container.hostname)){
                                      var now = moment();
                                      (nodes.find(l => l.nodeID === container.nodeID)).time = now;  
                                          
                                    }
                                    else{
                                      // if it doesnt exist we will update the node
                                        nodes.push(container);
                                     
                                    }
                                    print_nodes();

                                }, {
                                                noAck: true
                                            });
                });
});

}


function print_nodes(){

  console.log("This are the current nodes : ", nodes);

}


setInterval(function(){
// set up the leader
  console.log("This is the new leader");
  var maximun = get_leader(); 
  nodes.forEach((e) =>{
    if (e.nodeID == maximun){
      e.leader = true;
    }else{
      e.leader = false;
    }
  })


},8000);

// with the map and Math methods we are going to return the maximun id
function get_leader(){
  var maxC = Math.max(...nodes.map(o=>o.nodeID)); 
  return maxC;
 
}



setInterval(function(){

  if(toSend.nodeID == get_leader()){
    console.log("This is the container therefore we can check other nodes :", toSend);
    get_time(toSend);
  }else console.log("This is not the leader");


},9000);


//get the difference of the time
// if it is 2 minutes we have to check it if it is dead
// we use Math.abs because we dont want to handle with negative numbers
function get_time(lead){
  var now = moment();
  nodes.forEach((e) =>{
    var diff = now.diff(e.time,'minutes');
    if(Math.abs(diff) >=2){
      restart_node(e);
    }
  })
}

async function restart_node(element){
  // this function will restart the node
  // first we need to check if it is dead if it is not dead we just update the time, because that would mean that the node is not dead.
  var now = moment();  
  var get_host = element.hostname
  var get_id = element.nodeID
  console.log("This node is dead :", element);
  
  
  try{
    let res = await axios.get(`http://192.168.56.30:2375/containers/${get_host}/json`);
      
    var status= await res.data.State.Status;
    if(status == "exited"){
      await axios.post(`http://192.168.56.30:2375/containers/${get_host}/restart`).then(function(response){console.log(response)});
      toSend.dead= get_id;
    }else element.time = now;
   
    }
    catch(error)
    {
        console.log(error);
    }
        
 }
 
 const newContainer = "cloud-course-work_node4";

const nodeDetails = {
  Image: "cloud-course-work_node1",    
  Hostname: "cloud-coursework_new",
  NetworkingConfig: {
    EndpointsConfig: {
      "cloud-course-work_default": {},
    },
  },
    };

   



async function startNode(){
  // only create a new contianer if you are on the leader
  if(toSend.nodeID == get_leader()){

  
  
  console.log("Create a new container if it is the leade")
  try{
    
      await axios.post(`http://192.168.56.30:2375/containers/create?name=${newContainer}`, nodeDetails).then(function(response){console.log(response.data)});
        
      await axios.post(`http://192.168.56.30:2375/containers/${newContainer}/start`).then(function(response){console.log(response.data)});
           
      console.log("Docker ls to check if it was created");
          
      }
      catch(error)
      {
          console.log(error);
      }

    }
  }


  // async function removeNode(){
  
  //   console.log("Delete node");
  //   if(toSend.nodeID == get_leader()){
  
  //   try{
      
  //       await axios.delete(`http://192.168.56.30:2375/containers/${newContainer}`).then(function(response){console.log(response.data)});
          
       
             
  //       
  //       toSend.dead =  newContainer;
            
  //       }
  //       catch(error)
  //       {
  //           console.log(error);
  //       }
  //     }
  //   }
// with the schedule library we are going to create a node at an specific time
  const rule = new schedule.RecurrenceRule();
  rule.hour = 17;
  rule.minute = 00;
  
  // with this we are going to start the node at 17:00
  const newNode = schedule.scheduleJob(rule, function(){
    startNode();
  });
// we try to do the same for killing the node, however it was not working


  // const rules = new schedule.RecurrenceRule();
  // rules.hour = 18;
  // rules.minute = 00;
  
  
  // const killNode = schedule.scheduleJob(rules, function(){
  //   removeNode();
  // });









  newNode;
  // killNode;

 